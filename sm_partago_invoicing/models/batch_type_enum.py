from enum import Enum


class BatchType(Enum):
  TELETAC = "teletac"
  USAGES = "usage"
