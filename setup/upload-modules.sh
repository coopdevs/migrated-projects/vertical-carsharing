for dir in */; do
    cd "$dir" && python setup.py bdist_wheel && twine upload dist/*
    cd ..
done
