import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

def create_team(project):
  team_data = {
    'name': project.name,
    'alias_contact': 'everyone',
  }
  if project.user_id:
    team_data['user_ids'] = [(4, project.user_id.id)]
  if project.alias_name:
    team_data['alias_name'] = project.alias_name
    mail = c.model('mail.alias').browse([('alias_name', '=', project.alias_name)])
    mail.write({'alias_name':None})
  return c.model('helpdesk.ticket.team').create(team_data)

def create_tickets_from_tasks(project, team, ticket_type):
  for task in project.task_ids:
    ticket_data = {
      'name' : task.name,
      'type_id' : ticket_type.id,
      'team_id' : team.id,
      'sequence' : task.sequence,
      'description' : task.description or " ",
      'last_stage_update' : task.date_last_stage_update
    }
    if task.stage_id:
      if task.stage_id.name in ('ENTRADA', 'Inbox'):
        ticket_data['stage_id'] = 1
      elif task.stage_id.name in ('PENDENT', 'OBERT', 'OPEN'):
        ticket_data['stage_id'] = 2
        ticket_data['description'] = str(ticket_data['description']) + "\n- Project Stage: " + str(task.stage_id.name)
      elif task.stage_id.name in ('ESPERANT RESPOSTA'):
        ticket_data['stage_id'] = 3
      elif task.stage_id.name in ('TANCAT'):
        ticket_data['stage_id'] = 4
      elif task.stage_id.name in ('CANCELAT'):
        ticket_data['stage_id'] = 5
      else:
        print("No stage found for task: " + str(task.name) + "with ID: " + str(task.id))
        ticket_data['stage_id'] = 6 # Stage not found
        ticket_data['description'] = str(ticket_data['description']) + "\n- Project Stage: " + str(task.stage_id.name)
    else:
      print("No stage found for task: " + str(task.name) + "with ID: " + str(task.id))
      ticket_data['stage_id'] = 6 # Stage not found

    if task.user_id and ticket_data['stage_id'] != 1:
      ticket_data['user_id'] = task.user_id.id
    if task.partner_id:
      ticket_data['partner_id'] = task.partner_id.id

    c.model('helpdesk.ticket').create(ticket_data)

def delete_all_projects_and_tasks(projects):
  for project in projects:
    try:
      for task in project.task_ids:
        task.unlink()
      mail = project.alias_id
      project.unlink()
      mail.unlink()
    except Exception as err:
      print('Could not delete project: ' + str(project.name))
      continue

task_type_helpdesk = c.model('project.type').browse([('name', '=', 'Helpdesk')])
if not task_type_helpdesk:
  raise RuntimeError('No task type helpdesk found')

ticket_type_atencio = c.model('helpdesk.ticket.type').create({'name':'Atenció al client'})
ticket_type_gestio = c.model('helpdesk.ticket.type').create({'name':'Gestió'})

c.model('helpdesk.ticket.stage').create({'name':'Stage not found', 'sequence':6})

projects = c.model('project.project').browse([
  ('type_id', '=', task_type_helpdesk[0].id)])

for project in projects:
  team = create_team(project)

  if project.user_id.email == 'alfredo.cano@sommobilitat.coop':
    ticket_type = ticket_type_gestio
  else:
    ticket_type = ticket_type_atencio
  
  create_tickets_from_tasks(project, team, ticket_type)

delete_all_projects_and_tasks(projects)
