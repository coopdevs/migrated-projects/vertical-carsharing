import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

product = c.model('product.product').browse([('name', '=', 'Quota de soci')])
if product:
  product = product[0]
  product.is_share = True
  product.default_code = "SOCI"
  product.short_name = "Quota Soci"
else:
  print('ERROR: Could not find Quota de soci product')
  exit()

partners = c.model('res.partner').browse([('member', '=', True)], order='id ASC')

sub_register_obj = c.model("subscription.register")
sequence_operation = c.model('ir.sequence').browse([('code','=', 'subscription.register')])
share_line_obj = c.model("share.line")

for partner in partners:
  sub_reg_operation = sequence_operation.next_by_id()
  sub_reg_vals = {
    "partner_id": partner.id,
    "quantity": 1,
    "share_product_id": product.id,
    "share_unit_price": 10.0,
    "date": partner.create_date,
    "type": "subscription",
    "name": sub_reg_operation,
    "register_number_operation": int(sub_reg_operation)
  }
  share_values = {
    "share_product_id": product,
    "share_number": 1,
    "share_short_name": product.short_name,
    "share_unit_price": 10.0,
    "effective_date": partner.create_date,
    "partner_id": partner.id,
  }
  sub_register_obj.create(sub_reg_vals)
  share_line_obj.create(share_values)
