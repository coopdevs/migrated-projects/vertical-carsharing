import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
partners = c.model('res.partner').browse([('member_nr', '!=', False)])
for partner in partners: 
  partner.cooperator_register_number = partner.member_nr
  if partner.cooperator_register_number > 0:
    # Add effective date = creation date and cooperator type
    partner.member = True
    partner.cooperator = True