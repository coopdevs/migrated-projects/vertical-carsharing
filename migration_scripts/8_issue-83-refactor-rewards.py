import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
# Compute new data
rewards = c.model('sm_rewards.sm_reward').search([])
for reward_id in rewards:
  reward = c.model('sm_rewards.sm_reward').browse(reward_id)
  if reward.wp_coupon_id:
    reward.external_promo_obj_id = reward.wp_coupon_id
  if reward.reward_type == 'promocode':
    reward.data_partner_creation_type = 'none'
    if reward.reward_info == 'promo Montepio':
      reward.data_partner_cs_user_type = 'user'
    else:
      reward.data_partner_cs_user_type = 'promo'
    if reward.wp_member_coupon_id:
      reward.external_obj_id = reward.wp_member_coupon_id
      reward.data_partner_creation_type = 'existing'
    if reward.wp_member_id:
      reward.external_obj_id = reward.wp_member_id
      reward.data_partner_creation_type = 'new'
  if reward.reward_type == 'fleet_maintenance':
    reward.data_partner_creation_type = 'existing'
    if reward.maintenance_wp_entry_id:
      reward.external_obj_id = reward.maintenance_wp_entry_id
# Delete old cron
crons = c.model('ir.cron').browse([('name', 'ilike', 'Fetch and complete www rewards')])
for cron in crons:
  cron.unlink()