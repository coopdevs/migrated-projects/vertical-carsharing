import erppeek
from configparser import ConfigParser

config = ConfigParser()
config.read('erppeek.ini')
c = erppeek.Client.from_config('local')
places_list = c.model('crm.team').browse(['|',('team_type','=','map_place'),('team_type','=','map_place_proposal')])
for place in places_list:
  print("Executing: ",place.name)
  # generate meta
  place.build_presenter_metadata_ids()
  # subscribe follower
  place.message_subscribe([9])
