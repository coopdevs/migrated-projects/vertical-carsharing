import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

partners = c.model('res.partner').browse([('birthday', '!=', False)])

for partner in partners:
  partner.birthdate_date = partner.birthday