import erppeek
import pytz
import requests
from datetime import datetime
from configparser import ConfigParser

def _local_to_utc_datetime(local_date_str):
  mad_tz = pytz.timezone('Europe/Madrid')
  utc_tz = pytz.timezone('UTC')
  local_date = datetime.strptime(local_date_str, "%Y-%m-%d %H:%M:%S")
  local_date_localized = mad_tz.localize(local_date)
  return local_date_localized.astimezone(utc_tz)

def _local_to_utc_datetime_api(local_date_str):
  mad_tz = pytz.timezone('Europe/Madrid')
  utc_tz = pytz.timezone('UTC')
  local_date = datetime.strptime(local_date_str, "%Y-%m-%dT%H:%M:%S")
  local_date_localized = mad_tz.localize(local_date)
  return local_date_localized.astimezone(utc_tz)

def _validate_api_response(response):
    if response:
      if response.status_code != 200:
        return False
      return response.json()
    return False

def _get_reservations_api(api_url,api_key,from_q,till_q,group_q):
  return _validate_api_response(
    requests.get(
      api_url+'reservations',
      params={'from': from_q,'till': till_q, 'group': group_q},
      headers = {'Content-type': 'application/json', 'apiKey': api_key}
    )
  )

config = ConfigParser()
config.read('erppeek.ini')
c = erppeek.Client.from_config('local')

app_groups = c.model('smp.sm_group').search()
for app_group_id in app_groups:
  group = c.model('smp.sm_group').browse(app_group_id)
  reservations = _get_reservations_api(
    config.get('api','url'),
    config.get('api', 'key'),
    '2021-01-01T00:00:00.00',
    datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
    group.name
  )
  if reservations:
    for reservation_data in reservations:
      reservation_model = c.model('smp.sm_reservation_compute').browse([('name','=',reservation_data['id'])])
      if reservation_model:
        print('updating creation dates for: ', reservation_data['id'])
        update_values ={}
        if 'createdAt' in reservation_data.keys():
          update_values['creation_date_app'] = _local_to_utc_datetime_api(str(reservation_data['createdAt']).split('.')[0])
        if 'lastModifiedAt' in reservation_data.keys():
          update_values['lastmodified_date_app'] = _local_to_utc_datetime_api(str(reservation_data['lastModifiedAt']).split('.')[0])
        reservation_model.write(update_values)

reservations_q = c.model('smp.sm_reservation_compute').search()
for reservation_id in reservations_q:
  reservation = c.model('smp.sm_reservation_compute').browse(reservation_id)
  print('updating dates for: ', reservation_id)
  update_values = {}
  if reservation.startTime:
    update_values['startTime'] = _local_to_utc_datetime(str(reservation.startTime))
  if reservation.endTime:
    update_values['endTime'] = _local_to_utc_datetime(str(reservation.endTime))
  if reservation.effectiveStartTime:
    update_values['effectiveStartTime'] = _local_to_utc_datetime(str(reservation.effectiveStartTime))
  if reservation.effectiveEndTime:
    update_values['effectiveEndTime'] = _local_to_utc_datetime(str(reservation.effectiveEndTime))
  if update_values:
    reservation.write(update_values)


# update only one record
# reservations = c.model('smp.sm_reservation_compute').browse([('name','=','-Mw6u2869O90vcO-Ai04')])
# reservation = reservations[0]
# update_values = {}
# if reservation.startTime:
#   print(str(reservation.startTime))
#   update_values['startTime'] = _local_to_utc_datetime(str(reservation.startTime))
# if reservation.endTime:
#   update_values['endTime'] = _local_to_utc_datetime(str(reservation.endTime))
# if reservation.effectiveStartTime:
#   update_values['effectiveStartTime'] = _local_to_utc_datetime(str(reservation.effectiveStartTime))
# if reservation.effectiveEndTime:
#   update_values['effectiveEndTime'] = _local_to_utc_datetime(str(reservation.effectiveEndTime))
# if update_values:
#   reservation.write(update_values)